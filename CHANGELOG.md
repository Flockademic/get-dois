# Changelog

This project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [3.2.0] - 2022-09-17

### New features

- Articles on Open Journal Systems that do not expose a DOI in the metadata can now still be recognised, as long as the document body is passed as well.

## [3.1.0] - 2019-12-09

### New features

- DC.identifier.DOI tags are now supported. In concrete terms, this should mean that more items on Figshare should now be recognised.

## [3.0.0] - 2019-08-17

### Breaking changes

- `DC.relation` meta tags are no longer supported - they should not be used to identify the primary document.

## [2.0.0] - 2019-07-19

### New features

- More ways of recognising DOI metadata are now supported! Specifically, the markup used by Pubpub is now recognised by get-dois as well. (Technical details: get-dois now also detects meta tags that label themselves as DOI metadata using RDFa (i.e. using the `property` attribute rather than `name`).)

### Breaking changes

- If you're relying on a specific set of DOIs being detected, this release is breaking: more representations of DOIs can now be found. Altough I did not do that in the past, I'll be marking that as breaking from now on as well.

## [1.1.1] - 2019-01-17

### Changed

- Had to cut a new release due to a mistake in the release script. This should not impact consumers.

## [1.1.0] - 2019-01-17

### New features

- PRISM tags are now supported.

## [1.0.2] - 2018-11-20

### Bugfixes

- More invalid DOIs (whose registrant code is too short) are now rejected.

## [1.0.1] - 2018-11-19

### Bugfixes

- Remove the requirement for Node 8 or Yarn 1.

## [1.0.0] - 2018-11-15

### New features

- First release! Provide a DOM tree, receive an array (potentially empty) of DOIs listed on the page. Supports generic Dublin Core, Highwire Press tags, Eprints tags and BE Press tags.
