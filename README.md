get-dois
======
A small module that takes a DOM tree and returns all [Digital Object Identifiers](https://www.doi.org/) (DOIs) located in that tree's metadata.

Unfortunately, academic publishers use a wide variety of metadata "standards". This package normalises between Dublin Core, Highwire Press tags, [etc.](#supported-meta-tags), and the different formatting rules used for DOIs.

To use this library outside of a browser context, you can use a library like [jsdom](https://www.npmjs.com/package/jsdom).

This project also includes TypeScript type definitions.

## Installation

```bash
npm install get-dois --save
```

## Usage

```javascript
import { getDois } from 'get-dois';

const head = document.getElementsByTagName('head').item(0);
const dois = getDois(head);
//=> [ '10.31222/osf.io/796tu' ]
```

## API

### `getDois(domTree: Document | Element): string[]`

#### Parameter: `domTree`
Either `window.document` or an HTML element whose children might include meta tags containing a DOI.

#### Returns
An array of DOIs. If no DOIs are found, this will return an empty array. Duplicate DOIs will only be listed once. Note that a page might list multiple DOIs.

## Supported meta tags

At the time of writing, `get-dois` supports detecting DOIs embedded in a webpage as:

- Highwire Press tags (`citation_doi`)
- Eprints tags (`eprints.id_number` and/or `eprints.official_url`)
- bepress tags (`bepress_citation_doi`)
- PRISM tags (`prism.doi`)
- Dublin Core Schema markup (`dc.identifier`)

If you encounter a different format that is not yet supported, please [file an issue](https://gitlab.com/Flockademic/get-dois/issues/new).

## Changelog

See [`CHANGELOG.md`](https://gitlab.com/Flockademic/get-dois/blob/master/CHANGELOG.md).

## License

MIT © [Vincent Tunru](https://vincenttunru.com)
