import { JSDOM } from 'jsdom';

import { getDois } from './index';

it('should return an empty array when no DOI is present', () => {
  const dom = new JSDOM('<!DOCTYPE html><p>No DOIs here!</p>');
  const document = dom.window.document;

  expect(getDois(document)).toEqual([]);
});

it('should parse rAndOm-CAsed meta attributes', () => {
  const dom = new JSDOM(
    `<!DOCTYPE html><html><head>
      <meta name="dC.ideNtiFIer" content="10.31219/osf.io/khbvy">
    </head><p>Hello there</p></html>`
  );
  const document = dom.window.document;

  expect(getDois(document)).toEqual(['10.31219/osf.io/khbvy']);
});

it('should remove duplicate DOIs', () => {
  const dom = new JSDOM(
    `<!DOCTYPE html><html><head>
      <meta name="dc.identifier" content="doi:10.31219/osf.io/khbvy">
      <meta name="citation_doi" content="10.31219/osf.io/khbvy">
    </head><p>Hello there</p></html>`
  );
  const document = dom.window.document;

  expect(getDois(document)).toEqual(['10.31219/osf.io/khbvy']);
});

it('should accept partial DOM trees', () => {
  const dom = new JSDOM(
    `<!DOCTYPE html><html><head>
      <meta name="dc.identifier" content="doi:10.31219/osf.io/khbvy">
    </head><p>Hello there</p></html>`
  );
  const domTree = dom.window.document.getElementsByTagName('head').item(0)!;

  expect(getDois(domTree)).toEqual(['10.31219/osf.io/khbvy']);
});

describe('Recognising DOIs', () => {
  it('should not return identifiers whose registrant code is too short', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="dc.identifier" content="10.312/osf.io/khbvy">
      </head><p>No DOIs here!</p></html>`
    );
    const document = dom.window.document;
  
    expect(getDois(document)).toEqual([]);
  });

  it('should find valid identifiers', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="dc.identifier" content="10.31219/osf.io/khbvy">
      </head><p>Hello there</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual(['10.31219/osf.io/khbvy']);
  });
});

describe('Detecting DOIs located in dc.identifier meta tags', () => {
  it('should not return identifiers that are not DOIs', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="dc.identifier" content="https://osf.io/khbvy/">
      </head><p>No DOIs here!</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual([]);
  });

  it('should not return identifiers that are marked as something other than DOIs', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta scheme="not-doi" name="dc.identifier" content="10.31219/osf.io/khbvy">
      </head><p>No DOIs here!</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual([]);
  });

  it('should return a naked DOI', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="dc.identifier" content="10.31219/osf.io/khbvy">
      </head><p>Hello there</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual(['10.31219/osf.io/khbvy']);
  });

  it('should return a DOI prefixed with `doi:`', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="dc.identifier" content="doi:10.31219/osf.io/khbvy">
      </head><p>Hello there</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual(['10.31219/osf.io/khbvy']);
  });

  it('should return a DOI prefixed with `info:doi:`', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="dc.identifier" content="info:doi:10.31219/osf.io/khbvy">
      </head><p>Hello there</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual(['10.31219/osf.io/khbvy']);
  });

  it('should return multiple DOIs when present', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="dc.identifier" content="info:doi:10.31219/osf.io/khbvy">
        <meta name="dc.identifier" content="10.1371/journal.pbio.1002456">
      </head><p>Hello there</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual(['10.31219/osf.io/khbvy', '10.1371/journal.pbio.1002456']);
  });
});

describe('Detecting DOIs located in citation_doi meta tags', () => {
  it('should not return identifiers that are not DOIs', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="citation_doi" content="https://osf.io/khbvy/">
      </head><p>No DOIs here!</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual([]);
  });

  it('should return a naked DOI', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="citation_doi" content="10.31219/osf.io/khbvy">
      </head><p>Hello there</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual(['10.31219/osf.io/khbvy']);
  });

  it('should return a DOI prefixed with `doi:`', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="citation_doi" content="doi:10.31219/osf.io/khbvy">
      </head><p>Hello there</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual(['10.31219/osf.io/khbvy']);
  });

  it('should return a DOI prefixed with `info:doi:`', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="citation_doi" content="info:doi:10.31219/osf.io/khbvy">
      </head><p>Hello there</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual(['10.31219/osf.io/khbvy']);
  });

  it('should return multiple DOIs when present', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="citation_doi" content="info:doi:10.31219/osf.io/khbvy">
        <meta name="citation_doi" content="10.1371/journal.pbio.1002456">
      </head><p>Hello there</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual(['10.31219/osf.io/khbvy', '10.1371/journal.pbio.1002456']);
  });
});

describe('Detecting DOIs located in eprints.id_number meta tags', () => {
  it('should not return identifiers that are not DOIs', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="eprints.id_number" content="https://osf.io/khbvy/">
      </head><p>No DOIs here!</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual([]);
  });

  it('should return a naked DOI', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="eprints.id_number" content="10.31219/osf.io/khbvy">
      </head><p>Hello there</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual(['10.31219/osf.io/khbvy']);
  });

  it('should return a DOI prefixed with `doi:`', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="eprints.id_number" content="doi:10.31219/osf.io/khbvy">
      </head><p>Hello there</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual(['10.31219/osf.io/khbvy']);
  });

  it('should return a DOI prefixed with `info:doi:`', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="eprints.id_number" content="info:doi:10.31219/osf.io/khbvy">
      </head><p>Hello there</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual(['10.31219/osf.io/khbvy']);
  });

  it('should return multiple DOIs when present', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="eprints.id_number" content="info:doi:10.31219/osf.io/khbvy">
        <meta name="eprints.id_number" content="10.1371/journal.pbio.1002456">
      </head><p>Hello there</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual(['10.31219/osf.io/khbvy', '10.1371/journal.pbio.1002456']);
  });
});

describe('Detecting DOIs located in eprints.official_url meta tags', () => {
  it('should not return identifiers that are not DOIs', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="eprints.official_url" content="http://eprints.bbk.ac.uk/19432/">
      </head><p>No DOIs here!</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual([]);
  });

  it('should return a naked DOI', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="eprints.official_url" content="10.31219/osf.io/khbvy">
      </head><p>Hello there</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual(['10.31219/osf.io/khbvy']);
  });

  it('should return a DOI prefixed with `https://doi.org/`', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="eprints.official_url" content="https://doi.org/10.3138/jsp.49.1.26">
      </head><p>Hello there</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual(['10.3138/jsp.49.1.26']);
  });

  it('should return a DOI prefixed with `http://doi.org/`', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="eprints.official_url" content="http://doi.org/10.3138/jsp.49.1.26">
      </head><p>Hello there</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual(['10.3138/jsp.49.1.26']);
  });

  it('should return a DOI prefixed with `https://dx.doi.org/`', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="eprints.official_url" content="https://dx.doi.org/10.3138/jsp.49.1.26">
      </head><p>Hello there</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual(['10.3138/jsp.49.1.26']);
  });

  it('should return a DOI prefixed with `http://dx.doi.org/`', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="eprints.official_url" content="http://dx.doi.org/10.3138/jsp.49.1.26">
      </head><p>Hello there</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual(['10.3138/jsp.49.1.26']);
  });
});

describe('Detecting DOIs located in bepress_citation_doi meta tags', () => {
  it('should not return identifiers that are not DOIs', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="bepress_citation_doi" content="https://osf.io/khbvy/">
      </head><p>No DOIs here!</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual([]);
  });

  it('should return a naked DOI', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="bepress_citation_doi" content="10.31219/osf.io/khbvy">
      </head><p>Hello there</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual(['10.31219/osf.io/khbvy']);
  });

  it('should return a DOI prefixed with `doi:`', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="bepress_citation_doi" content="doi:10.31219/osf.io/khbvy">
      </head><p>Hello there</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual(['10.31219/osf.io/khbvy']);
  });

  it('should return a DOI prefixed with `info:doi:`', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="bepress_citation_doi" content="info:doi:10.31219/osf.io/khbvy">
      </head><p>Hello there</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual(['10.31219/osf.io/khbvy']);
  });

  it('should return multiple DOIs when present', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="bepress_citation_doi" content="info:doi:10.31219/osf.io/khbvy">
        <meta name="bepress_citation_doi" content="10.1371/journal.pbio.1002456">
      </head><p>Hello there</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual(['10.31219/osf.io/khbvy', '10.1371/journal.pbio.1002456']);
  });
});

describe('Detecting DOIs located in prism.doi meta tags', () => {
  it('should not return identifiers that are not DOIs', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="prism.doi" content="https://osf.io/khbvy/">
      </head><p>No DOIs here!</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual([]);
  });

  it('should return a naked DOI', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="prism.doi" content="10.31219/osf.io/khbvy">
      </head><p>Hello there</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual(['10.31219/osf.io/khbvy']);
  });

  it('should return a DOI prefixed with `doi:`', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="prism.doi" content="doi:10.31219/osf.io/khbvy">
      </head><p>Hello there</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual(['10.31219/osf.io/khbvy']);
  });

  it('should return a DOI prefixed with `info:doi:`', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="prism.doi" content="info:doi:10.31219/osf.io/khbvy">
      </head><p>Hello there</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual(['10.31219/osf.io/khbvy']);
  });

  it('should return multiple DOIs when present', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="prism.doi" content="info:doi:10.31219/osf.io/khbvy">
        <meta name="prism.doi" content="10.1371/journal.pbio.1002456">
      </head><p>Hello there</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual(['10.31219/osf.io/khbvy', '10.1371/journal.pbio.1002456']);
  });
});

// DC.identifier.DOI is used by some Figshare pages. See
// https://gitlab.com/Flockademic/get-dois/issues/7
describe('Detecting DOIs located in DC.identfier.DOI meta tags', () => {
  it('should not return identifiers that are not DOIs', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="DC.identifier.DOI" content="https://osf.io/khbvy/">
      </head><p>No DOIs here!</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual([]);
  });

  it('should return a naked DOI', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="DC.identifier.DOI" content="10.31219/osf.io/khbvy">
      </head><p>Hello there</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual(['10.31219/osf.io/khbvy']);
  });

  it('should return a DOI prefixed with `doi:`', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="DC.identifier.DOI" content="doi:10.31219/osf.io/khbvy">
      </head><p>Hello there</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual(['10.31219/osf.io/khbvy']);
  });

  it('should return a DOI prefixed with `info:doi:`', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="DC.identifier.DOI" content="info:doi:10.31219/osf.io/khbvy">
      </head><p>Hello there</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual(['10.31219/osf.io/khbvy']);
  });

  it('should return multiple DOIs when present', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="DC.identifier.DOI" content="info:doi:10.31219/osf.io/khbvy">
        <meta name="DC.identifier.DOI" content="10.1371/journal.pbio.1002456">
      </head><p>Hello there</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual(['10.31219/osf.io/khbvy', '10.1371/journal.pbio.1002456']);
  });
});

describe('Detecting DOIs located in meta tags identified by the `property` attribute', () => {
  it('should not return identifiers that are not DOIs', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta property="dc.identifier" content="https://osf.io/khbvy/">
      </head><p>No DOIs here!</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual([]);
  });

  it('should not return identifiers that are marked as something other than DOIs', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta scheme="not-doi" property="dc.identifier" content="10.31219/osf.io/khbvy">
      </head><p>No DOIs here!</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual([]);
  });

  it('should return a naked DOI', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta property="dc.identifier" content="10.31219/osf.io/khbvy">
      </head><p>Hello there</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual(['10.31219/osf.io/khbvy']);
  });

  it('should return a DOI prefixed with `doi:`', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta property="dc.identifier" content="doi:10.31219/osf.io/khbvy">
      </head><p>Hello there</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual(['10.31219/osf.io/khbvy']);
  });

  it('should return a DOI prefixed with `info:doi:`', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta property="dc.identifier" content="info:doi:10.31219/osf.io/khbvy">
      </head><p>Hello there</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual(['10.31219/osf.io/khbvy']);
  });

  it('should return multiple DOIs when present', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta property="dc.identifier" content="info:doi:10.31219/osf.io/khbvy">
        <meta property="dc.identifier" content="10.1371/journal.pbio.1002456">
      </head><p>Hello there</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual(['10.31219/osf.io/khbvy', '10.1371/journal.pbio.1002456']);
  });

  it('should return multiple DOIs when present, whether identified by `name` or `property`', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="dc.identifier" content="info:doi:10.31219/osf.io/khbvy">
        <meta property="dc.identifier" content="10.1371/journal.pbio.1002456">
      </head><p>Hello there</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual(['10.31219/osf.io/khbvy', '10.1371/journal.pbio.1002456']);
  });

  it('should not choke on both a name and property attribute being defined', () => {
    const dom = new JSDOM(
      `<!DOCTYPE html><html><head>
        <meta name="dc.identifier" property="dc.identifier" content="10.31219/osf.io/khbvy">
      </head><p>Hello there</p></html>`
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual(['10.31219/osf.io/khbvy']);
  });
});

describe('Detecting DOIs located in known elements in the <body>', () => {
  it('returns DOIs listed for articles on Open Journal Systems', () => {
    const dom = new JSDOM(
      // Stripped DOM from an actual OJS site:
      `
        <div class="pkp_structure_content has_sidebar">
          <div class="pkp_structure_main" role="main">
            <a id="pkp_content_main"></a>
            <div class="page page_article">
              <article class="obj_article_details">
                <div class="row">
                  <div class="main_entry">
                    <section class="item doi">
                      <h2 class="label">
                        DOI:
                      </h2>
                      <span class="value">
                        <a href="https://doi.org/10.31219/osf.io/khbvy">
                          https://doi.org/10.31219/osf.io/khbvy
                        </a>
                      </span>
                    </section>
                  </div><!-- .main_entry -->
                  <!-- .entry_details -->
                </div><!-- .row -->
              </article>
            </div><!-- .page -->
          </div><!-- pkp_structure_main -->
        </div>
      `
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual(['10.31219/osf.io/khbvy']);
  });

  it('does not return a DOI for articles on Open Journal Systems with invalid HTML', () => {
    const dom = new JSDOM(
      // Stripped DOM from an actual OJS site:
      `
        <div class="pkp_structure_content has_sidebar">
          <div class="pkp_structure_main" role="main">
            <a id="pkp_content_main"></a>
            <div class="page page_article">
              <article class="obj_article_details">
                <div class="row">
                  <div class="main_entry">
                    <section class="item doi">
                      <h2 class="label">
                        DOI:
                      </h2>
                      <span class="value">
                        <a data-comment="href attribute missing">
                          https://doi.org/10.31219/osf.io/khbvy
                        </a>
                      </span>
                    </section>
                  </div><!-- .main_entry -->
                  <!-- .entry_details -->
                </div><!-- .row -->
              </article>
            </div><!-- .page -->
          </div><!-- pkp_structure_main -->
        </div>
      `
    );
    const document = dom.window.document;

    expect(getDois(document)).toEqual([]);
  });
});
