import './polyfills';

const identifierAttributes = [
  'dc.identifier',
  'citation_doi',
  'eprints.id_number',
  'eprints.official_url',
  'bepress_citation_doi',
  'prism.doi',
  'dc.identifier.doi',
];

function parseDoi(identifier: string): string | null {
  const withoutScheme = identifier.replace(/^((info:)?doi:)|(https?:\/\/(dx\.)?doi.org\/)/, '');

  // A DOI is `10.`, followed by a number of digits, then a slash, and then any number of characters
  return /^10\.\d{4,}\/.*$/.test(withoutScheme) ? withoutScheme : null;
}

function isNotNull<T>(value: T | null): value is T {
  return value !== null;
}

export function getDois(domTree: Document | Element) {
  const metaNameTags = Array.from(domTree.querySelectorAll('meta[name]'));
  const metaNameTagsWithIdentifier = metaNameTags
    .filter(tag => identifierAttributes.indexOf(tag.getAttribute('name')!.toLowerCase()) !== -1)
  const metaPropertyTags = Array.from(domTree.querySelectorAll('meta[property]'));
  const metaPropertyTagsWithIdentifier = metaPropertyTags
    .filter(tag => identifierAttributes.indexOf(tag.getAttribute('property')!.toLowerCase()) !== -1)

  const metaTagsWithIdentifier = metaNameTagsWithIdentifier.concat(metaPropertyTagsWithIdentifier)
    .filter(tag => {
      const scheme = tag.getAttribute('scheme');
      return scheme === null || scheme === '' || scheme.toLowerCase() === 'doi';
    });

  const identifiers = metaTagsWithIdentifier.map(tag => tag.getAttribute('content')).filter(isNotNull);

  // Open Journal Systems does not necessarily list the DOI in the <head>,
  // but if `domTree` includes the <body>, we can at least try to find the link
  // there:
  const ojsDoiLink = domTree.querySelector('.pkp_structure_main .doi .value a');
  if (ojsDoiLink) {
    const ojsDoiLinkHref = ojsDoiLink.getAttribute('href');
    if (typeof ojsDoiLinkHref === 'string') {
      identifiers.push(ojsDoiLinkHref);
    }
  }

  const dois = identifiers.map(parseDoi).filter(isNotNull);
  const uniqueDois = dois.filter((doi, index) => dois.indexOf(doi) === index);

  return uniqueDois;
}
